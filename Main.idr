module Main

main : IO ()
main = do
  args <- getArgs
  case args of
       (_ :: n :: s :: as) => putStr (concat (take (cast n) (repeat s)))
       _ => pure ()
