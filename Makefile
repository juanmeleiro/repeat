BIN_PATH = /usr/bin
MAN_PATH = /usr/share/man/man1
IDRIS    = $(HOME)/.cabal/bin/idris

build :
	$(IDRIS) Main.idr -o repeat

install : build
	mv repeat $(BIN_PATH)/repeat
	cp repeat.1 $(MAN_PATH)/repeat.1

clean :
	rm -f repeat Main.ibc
