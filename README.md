# repeat

This is a stupidly simple command-line utility for repeating a string a
specified number of times. I couldn't believe there was no simple way to do
that, so I made it.

Why Idris? Because I'm a nerd.

## Installation

```bash
$ sudo make clean install
```
